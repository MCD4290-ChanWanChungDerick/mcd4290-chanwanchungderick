// Question 1
let circumference=2*Math.PI*4;
console.log(circumference.toFixed(2));

// Question 2
let animalString = "cameldogcatlizard";
let andString = " and ";
console.log(animalString.substr(8,3)+ andString+animalString.substring(5,8))
 
// Question 3
let person ={
FirstName :"Kanye",
LastName :"West",
BirthDate :"8 June 1977",
AnnualIncome : "150000000",
};
console.log(person.FirstName+' '+person.LastName+' '+'was born on'+' '+person.BirthDate+' '+'and has an annual income of'+' '+person.AnnualIncome);

// Question 4
var number1, number2;
number1 = Math.floor((Math.random() * 10) + 1);
number2 = Math.floor((Math.random() * 10) + 1);
console.log("number1 = " + number1 + " number2 = " + number2);
swap1=number1;
number1=number2;
number2=swap1;
console.log("number1 = " + number1 + " number2 = " + number2);
 
// Question 5
let year;
let yearNot2015Or2016;
year = 2016;
yearNot2015Or2016 = (year != 2015) && (year != 2016);
console.log(yearNot2015Or2016);