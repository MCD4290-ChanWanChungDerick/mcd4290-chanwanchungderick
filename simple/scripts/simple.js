function doIt() {
    
    // get the number variable from the user
    var number1 = Number(document.getElementById("number1").value);
    var number2 = Number(document.getElementById("number2").value);
    var number3 = Number(document.getElementById("number3").value);

    // get the answer and type of the number
    var getAnswer = document.getElementById("answer");
    var getOddEven = document.getElementById("oddEven");

    // calculate the result
    var getResult = number1 + number2 + number3;

    // give color to positive and negative
    if (getResult < 0) {
        answer.className = "negative";
    } else {
        answer.className = "positive";
    }

    // give color to odd/even and return the answer to html
    if (getResult % 2 === 0) {
        getOddEven.innerHTML = "(Even)";
        getOddEven.className = "even";
    } else {
        getOddEven.innerHTML = "(Odd)";
        getOddEven.className = "odd";
    }
    
    // return the result to html
    getAnswer.innerHTML = getResult;
    
}