//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
   let myArray=[ 54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25];
    let PosOdd=[];
    let NegEven=[];
    for(let i=0;i<myArray.length;i++){
        if(myArray[i]>0 && myArray[i]%2 !==0){
            PosOdd.push(myArray[i])
        }
        else if (myArray[i]<0 && myArray[i]%2 ===0){
            NegEven.push(myArray[i])
        }
    }
    
    output += "Positve Odd: " + PosOdd ;
    output += "Negative Even: " + NegEven 
    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    
    //Question 2 here 
    var array = [0, 0, 0, 0, 0, 0];
var randomNumber;
    
for (var i = 0; i < 60000; i++) {   
    randomNumber = Math.floor(Math.random() * 6) + 1;
    if(randomNumber === 1){
      array[0]++;
    }
    else if(randomNumber === 2){
      array[1]++;
    }
    else if(randomNumber === 3){
      array[2]++;
    }
    else if(randomNumber === 4){
      array[3]++;
    }
    else if(randomNumber === 5){
      array[4]++;
    }
    else if(randomNumber === 6){
      array[5]++;
    }
}
output += "Frequency of die roll: \n" + "1:" + array[0] +
    "\n 2:" + array[1] +    "\n 3:" + array[2] +    "\n 4:" + array[3] +    "\n 5:" + array[4] +    "\n 6:" + array[5] ;
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    //Question 3 here 
    let frequencies =[0, 0, 0, 0, 0, 0, 0]
    
    for (i =0; i < 60000; i++){
        let r =Math.floor((Math.random()*6)+1)
        frequencies[r] +=1
    }
output += "Frequency of the die rolls"
output += "\n"
    for (let i=1; i<frequencies.length; i++){
        output += i+":" + frequencies[i]+"\n"
    }
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here 
    let dieRolls={
        Frequencies:{
            1:0,
            2:0,
            3:0,
            4:0,
            5:0,
            6:0, 
        },
        Total:60000,
        Exceptions:""
    }
    for(let i=0 ;i<dieRolls.Total;i++){
        let r=Math.floor((Math.random()*6)+1)
        dieRolls.Frequencies[r] += 1
    }
    output += "Frequency of die rolls"
    
    for (let number in dieRolls.Frequencies){
        output += "\n"
        output += number + ":"
        output += dieRolls.Frequencies[number]
    }
    for (let numb in dieRolls.Frequencies){
        if (Math.abs(dieRolls.Frequencies[numb]-10000)>10000*0.01){dieRolls.Exceptions += numb
         dieRolls.Exceptions += " "                                                                                         }                    
    }
        output += "\n"
        output += "Exceptions:"
        output += dieRolls.Exceptions
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here 
    let person = { name: "Jane",
    income: 127050
}
    let tax = 0;
    if ( person.income<18200){
        tax=0;
    }else if (person.income<37000){
        tax=(person.income-18200)*0.19
    }else if (person.income<90000){
        tax=(3572+((person.income-37000)*0.325))
    }else if (person.income<180000){
        tax=(20797+((person.income-90000)*0.37))
    } else {
        tax=(54097+((person.income-180000)*0.45))
    }
    
    output += person.name + " income is " + person.income + " and her tax owed is " + tax ;
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}